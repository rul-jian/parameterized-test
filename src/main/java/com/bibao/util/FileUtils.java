package com.bibao.util;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {
	public static final String ROOT = "src/main/resources/";
	
	public static List<String> readFromCsvFile(String filename) {
		List<String> list = new ArrayList<>();
		try (FileReader reader = new FileReader(ROOT + filename);
			BufferedReader buffer = new BufferedReader(reader)) {
			String line = buffer.readLine();
			while (line!=null) {
				list.add(line);
				line = buffer.readLine();
			}
		} catch (IOException e) {
			System.err.println(e);
		}
		list.remove(0);
		return list;
	}
	
}
